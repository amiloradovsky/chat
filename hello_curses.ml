open Curses

let meh () = endwin ();
             print_endline "Meh. Something went wrong."; exit (-1)
and input_field_height = 3  (* the space for how many lines is needed *)

let or_die f x = if f x then () else meh ()

let () =
  let _ = initscr () in
  (* if cbreak () then ()
   * else (print_endline "Cannot disable line buffering!"; exit 1); *)

  or_die addstr "Hello World !!!";
  or_die refresh ();

  let lines, columns = get_size () in
  let w = newwin input_field_height columns
            (lines - input_field_height) 0 in
  let text = " client " in
  box w 0 0;
  or_die (mvwaddstr w 0 ((columns - String.length text) / 2)) text;
  or_die (wmove w 1) 2;
  or_die wrefresh w;

  let _ = getch () in
  endwin ()
