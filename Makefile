XML_LIGHT_PATH =
CURSES_PATH =

all: client server solution hello_curses

client: client.ml
	ocamlc -o $@ unix.cma $<

server: server.ml
	ocamlc -o $@ -I $(XML_LIGHT_PATH) -I +threads \
		unix.cma threads.cma xml-light.cma $<

solution: transmission.ml solution.ml
	ocamlc -o $@ -I +threads unix.cma threads.cma $^

hello_curses: hello_curses.ml
	ocamlfind ocamlopt -o $@ -I $(CURSES_PATH) -I +threads \
		unix.cmxa threads.cmxa curses.cmxa $<

install:
	mkdir -p /usr/bin/
	install solution client server hello_curses /usr/bin/

clean:
	rm {client,server,solution,hello_curses}{,.cm{i,o}}
