open Unix

let address = ref "localhost"
and port = ref 8989
and nick = ref ""

let rec main_loop sep in_ch out_ch =
  try let recv_line = input_line in_ch in
      if recv_line = sep then
        begin
          print_string !nick;
          print_string "< ";
          let send_line = read_line () in
          let time_0 = Unix.gettimeofday () in
          output_string out_ch (String.concat "" [send_line; "\n"]);
          flush out_ch;
          let response = input_line in_ch in
          let time_1 = Unix.gettimeofday () in
          if response = String.concat " " [sep; "message received"]
          then (print_string (string_of_float (time_1 -. time_0));
                print_endline " seconds.")
          else (print_endline "Wrong acknowledgment response!"; exit 2)
        end
      else
        print_endline (String.concat " " [">"; recv_line]);
      main_loop sep in_ch out_ch
  with End_of_file -> print_endline "Connection closed."

let parse_options () =
  let nick_candidate = ref None in
  let read_nick name = match !nick_candidate with
    | None -> nick_candidate := Some name
    | Some _ -> print_endline "You can specify the nickname only once!";
                exit 4
  and specs = ["-host", Arg.Set_string address,
               "specifies the name of the host to connect to";
               "-port", Arg.Set_int port,
               "specifies the number of the port to connect to"] in
  Arg.parse specs read_nick
    "Arguments: [-host <name>] [-port <number>] nickname";
  match !nick_candidate with
    | Some name -> nick := name
    | None -> print_endline "You have to specify a nickname!";
              exit 5

let () =
  parse_options ();
  let host = gethostbyname !address in
  let sock = socket host.h_addrtype SOCK_STREAM 0 in
  let test_connect addr =
    try connect sock (ADDR_INET (addr, !port)); true with _ -> false in
  if Array.exists test_connect host.h_addr_list then ()
  else (print_endline "Cannot connect!"; exit 3);
  let cin = in_channel_of_descr sock
  and cout = out_channel_of_descr sock in
  let () = output_string cout !nick; output_string cout "\n"; flush cout
  and sep = try input_line cin with
              End_of_file -> print_endline "Can't get the separator.";
                             exit 1 in
  main_loop sep cin cout;
  close sock
