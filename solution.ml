open Unix

type mode = Server | Client of string

let port = ref 9142  (* not a standard; off the top of my head... *)
and backlog = 3  (* small'ish value chosen for testing purposes *)
and escape_character = '\\'  (* the standard one for control purposes *)
and response_text = "message received"  (* should be random and long *)

let run_client address =
  let host = gethostbyname address in
  let sock = socket host.h_addrtype SOCK_STREAM 0 in
  let test_connect addr =
    try connect sock (ADDR_INET (addr, !port)); true with _ -> false in
  if Array.exists test_connect host.h_addr_list then ()
  else (print_endline "Cannot connect!"; exit 3);
  let _, z = Transmission.start_echoing
               escape_character response_text sock in
  Thread.join z

let run_server () =
  let sock = socket PF_INET6 SOCK_STREAM 0 in
  setsockopt sock SO_REUSEADDR true;
  bind sock (ADDR_INET (inet6_addr_any, !port));
  listen sock backlog;
  print_endline "Entering accept loop...";
  let rec accept_loop sock =
    let (s, _) = accept sock in
    print_endline "Accepted a connection.";
    let _, _ = Transmission.start_echoing
                 escape_character response_text s in
    accept_loop sock
  in accept_loop sock

let parse_options_and_go_off () =
  let conflicting_modes () =
    print_endline "You can specify the operation mode only once!";
    exit 1
  and anon_args_warning _ =
    print_endline "Anonymous option ignored."
  and pre_mode = ref None in
  let become_client addr =
    match !pre_mode with
    | None -> pre_mode := Some (Client addr)
    | Some _ -> conflicting_modes ()
  and become_server () =
    match !pre_mode with
    | None -> pre_mode := Some Server
    | Some _ -> conflicting_modes ()
  in
  let specs = ["-port", Arg.Set_int port,
               "specifies the number of the port to serve from";
               "-server", Arg.Unit become_server,
               "specifies the number of the port to serve from";
               "-client", Arg.String become_client,
               "specifies the number of the port to serve from"] in
  Arg.parse specs anon_args_warning
    "Arguments: {-server | -client <host>} [-port <number>]";
  match !pre_mode with
  | Some (Client address) -> run_client address
  | Some Server -> run_server ()
  | None -> print_endline "You have specify the operation mode!";
            exit 2

let () = parse_options_and_go_off ()
