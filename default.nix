let
  pkgs = import <nixpkgs> {};
  stdenv = pkgs.stdenv;
in rec {
  chat = stdenv.mkDerivation rec {
    name = "ahrefs-test";
    src = ./.;
    prePatch = with pkgs.ocamlPackages; ''
    substituteInPlace ./Makefile \
    --replace "XML_LIGHT_PATH =" "XML_LIGHT_PATH = ${xml-light}/lib/ocaml/${ocaml.version}/site-lib/xml-light" \
    --replace "CURSES_PATH =" "CURSES_PATH = ${curses}/lib/ocaml/${ocaml.version}/site-lib/curses" \
    --replace /usr $out
    '';

    createFindlibDestdir = true;

    buildInputs = with pkgs; [
      ocamlPackages.ocaml
      ocamlPackages.findlib
      ocamlPackages.xml-light
      ocamlPackages.curses
      stdenv
    ];
  };
}
