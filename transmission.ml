open Unix

let delay = 1.  (* artificial delay, in seconds
                   used for testing the queuing *)

let output_endline channel string =
  output_string channel string;
  output_string channel "\n";
  flush channel

let inward_echo escape_character response_text
      channel_in channel_out mutex times_sent () =
  let output line = print_endline line;  (* content for the user *)
                    output_char channel_out escape_character;
                    output_endline channel_out response_text
  and is_escaped line =
    (String.length line != 0) && (String.get line 0 = escape_character)
  and rest_of_line line =  (* assumed to be non-empty *)
    String.sub line 1 (String.length line - 1) in
  let parse_control line =
    if is_escaped line
    then (print_char escape_character; output (rest_of_line line))
    else let time_received = Unix.gettimeofday () in
         Mutex.lock mutex;
         let time_sent =
           try Queue.take times_sent with Queue.Empty ->
             print_endline "Haven't sent that much messages!";
             exit 4 in
         Mutex.unlock mutex;
         let diff = string_of_float (time_received -. time_sent) in
         print_endline (String.concat " "
                          ["Round trip"; diff; "seconds."]) in
  (* here we don't actually care about the control command itself,
     because there is just one at the moment, there's no ambiguity *)
  let rec parse_line line = if is_escaped line
                            then parse_control (rest_of_line line)
                            else output line in
  let line = input_line channel_in in
  Thread.delay delay;
  parse_line line
and outward_echo channel_out mutex times_sent () =
  let line = read_line () in
  Mutex.lock mutex;
  Queue.push (Unix.gettimeofday ()) times_sent;
  Mutex.unlock mutex;
  output_endline channel_out line

let start_echoing escape_character response_text sock =
  let is_active = ref true in
  let mutex = Mutex.create ()
  and queue = Queue.create ()
  and channel_in = in_channel_of_descr sock
  and channel_out = out_channel_of_descr sock in
  let on_start action =
    let rec try_echo action =
      if !is_active then (action (); try_echo action) else () in
    try try_echo action with End_of_file ->
      is_active := false;
      print_endline "Closing the connection.";
      close sock in
  let t0 = Thread.create on_start
             (inward_echo escape_character response_text
                channel_in channel_out mutex queue)
  and t1 = Thread.create on_start
             (outward_echo channel_out mutex queue)
  in t0, t1
